// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
// import apiConfig from '../config/api.config'
import Vue from 'vue'
import Axios from 'axios'
import VueAxios from 'vue-axios'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import { store } from './store'

Vue.use(Vuetify)
Vue.use(VueAxios, Axios)
Axios.defaults.baseURL = process.env.ROOT_API
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
  created () {
    store.dispatch('startLoading')
    const token = localStorage.getItem('NM-token')
    const username = localStorage.getItem('NM-username')
    if (token && username) {
      store.dispatch('autoSignIn', {
        token,
        username
      })
    }
    store.dispatch('stopLoding')
  }
})
