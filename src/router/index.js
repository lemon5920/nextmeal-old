import Vue from 'vue'
import Router from 'vue-router'
import defaultPage from '@/components/defaultPage'
import authentication from '@/components/authentication'
import restaurants from '@/components/restaurants'
import restaurantDetail from '@/components/restaurantDetail'
import orderFood from '@/components/orderFood'
import launchTour from '@/components/launchTour'
import receivable from '@/components/receivable'
import payable from '@/components/payable'
import insertRestaurants from '@/components/insertRestaurants'
import editRestaurants from '@/components/editRestaurants'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'defaultPage',
      component: defaultPage
    },
    {
      path: '/authentication',
      name: 'authentication',
      component: authentication
    },
    {
      path: '/restaurants',
      name: 'restaurants',
      component: restaurants
    },
    {
      path: '/restaurants/:id',
      name: 'restaurantDetail',
      component: restaurantDetail,
      props: true
    },
    {
      path: '/orderFood',
      name: 'orderFood',
      component: orderFood
    },
    {
      path: '/launchTour',
      name: 'launchTour',
      component: launchTour
    },
    {
      path: '/receivable',
      name: 'receivable',
      component: receivable
    },
    {
      path: '/payable',
      name: 'payable',
      component: payable
    },
    {
      path: '/insertRestaurants',
      name: 'insertRestaurants',
      component: insertRestaurants
    },
    {
      path: '/editRestaurants/:id',
      name: 'editRestaurants',
      component: editRestaurants
    }
  ]
})
