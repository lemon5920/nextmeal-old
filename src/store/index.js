import Vue from 'vue'
import Vuex from 'vuex'
import user from './moduleOfUser'
import common from './common'

Vue.use(Vuex)

export const store = new Vuex.Store({
  modules: {
    user,
    common
  }
})
