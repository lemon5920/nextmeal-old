const common = {
  namespced: true,
  state: {
    loading: false,
    snackbar: {
      show: false,
      top: true,
      multiLine: true,
      timeout: 2000,
      color: 'success',
      text: ''
    }
  },
  mutations: {
    setLoading (state, payload) {
      state.loading = payload
    },
    setSnackbar (state, payload) {
      state.snackbar.show = payload
    },
    setSnackbarColor (state, payload) {
      state.snackbar.color = payload || 'success'
    },
    setSnackbarText (state, payload) {
      state.snackbar.text = payload
    }

  },
  actions: {
    startLoading ({ commit }) {
      commit('setLoading', true)
    },
    stopLoding ({ commit }) {
      commit('setLoading', false)
    },
    showSnackbar ({commit}, payload) {
      commit('setSnackbarColor', payload.color)
      commit('setSnackbarText', payload.text)
      commit('setSnackbar', true)
    },
    hideSnackbar ({commit}) {
      commit('setSnackbar', false)
    }
  },
  getters: {}
}
export default common
