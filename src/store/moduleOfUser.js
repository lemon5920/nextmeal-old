const moduleOfUser = {
  namespced: true,
  state: {
    authToken: null,
    username: null
  },
  mutations: {
    setAuthToken (state, payload) {
      state.authToken = 'Bearer ' + payload
    },
    setUsername (state, payload) {
      state.username = payload
    }
  },
  actions: {
    signInSuccess ({ commit }, payload) {
      localStorage.setItem('NM-token', payload.token)
      localStorage.setItem('NM-username', payload.username)
      commit('setAuthToken', payload.token)
      commit('setUsername', payload.username)
    },
    signOut ({ commit }) {
      localStorage.removeItem('NM-token')
      localStorage.removeItem('NM-username')
      commit('setAuthToken', '')
      commit('setUsername', '')
    },
    autoSignIn ({ commit }, payload) {
      commit('setAuthToken', payload.token)
      commit('setUsername', payload.username)
    },
    signUpSuccess ({ commit }, payload) {
      localStorage.setItem('NM-token', payload.token)
      localStorage.setItem('NM-username', payload.username)
      commit('setAuthToken', payload.token)
      commit('setUsername', payload.username)
    }
  },
  getters: {}
}

export default moduleOfUser
