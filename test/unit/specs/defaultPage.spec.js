// import Vue from 'vue'
import defaultPage from '@/components/defaultPage'

describe('defaultPage', () => {
  it('has a created hook', () => {
    expect(typeof defaultPage.created).to.equal('function')
  })
})
