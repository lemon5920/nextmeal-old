module.exports = function (browser) {

  this.logoImageShouldBeVisible = () => {
    return browser
      .assert.elementPresent('.logoImage')
  };

};
