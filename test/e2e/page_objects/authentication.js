module.exports = function (browser) {

  this.cardShouldBeVisible = () => {
    return browser
      .assert.elementPresent('#card-auth')
  };

  this.clickSignInTab = () => {
    return browser
      .assert.elementPresent('#tab-signIn > a')
      .click('#tab-signIn > a')
  };

  this.clickSignUpTab = () => {
    return browser
      .assert.elementPresent('#tab-signUp > a')
      .click('#tab-signUp > a')
  };

  this.signInFormShouldBeVisible = () => {
    return browser
      .assert.elementPresent('#form-signIn')
      .waitForElementVisible('#form-signIn', 2000)
  };

  this.signUpFormShouldBeVisible = () => {
    return browser
      .assert.elementPresent('#form-signUp')
      .waitForElementVisible('#form-signUp', 2000)
  };

};
