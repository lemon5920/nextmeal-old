module.exports = function (browser) {

  this.showPage = (devServer) => {
    return browser
      .url(devServer)
      .waitForElementVisible('#app', 5000)
  };

};
