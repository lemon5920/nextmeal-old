module.exports = {

  'Default page should show the logo image.': function (browser) {
    const devServer = browser.globals.devServerURL
    browser
      .page.common().showPage(devServer)
      .page.defaultPage().logoImageShouldBeVisible()
      .end()
  }

}
