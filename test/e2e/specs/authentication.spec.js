module.exports = {

  'The card should be visible.': function (browser) {
    const devServer = browser.globals.devServerURL + '/authentication'
    browser
      .page.common().showPage(devServer)
      .page.authentication().cardShouldBeVisible()
      .end()
  },

  'Click signIn tab will show signIn form': function (browser) {
    const devServer = browser.globals.devServerURL + '/authentication'
    browser
      .page.common().showPage(devServer)
      .page.authentication().clickSignInTab()
      .page.authentication().signInFormShouldBeVisible()
      .end()
  },

  'Click signUp tab will show signUp form': function (browser) {
    const devServer = browser.globals.devServerURL + '/authentication'
    browser
      .page.common().showPage(devServer)
      .page.authentication().clickSignUpTab()
      .page.authentication().signUpFormShouldBeVisible()
      .end()
  }

}
